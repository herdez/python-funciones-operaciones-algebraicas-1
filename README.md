# Función Algebraica y Gráfica

En este reto es importante investigar el uso de la librería `matplotlib` y un poco de operaciones algebraicas.

Definir una función que permita graficar un intervalo de valores de `-100 a 100` como se muestra en la siguiente gráfica:

![](images/graph_1.png)

Dada la siguiente función algebraica:

<img src="images/formula.png" width="170" />

Restricciones:

 ```python
# Definir la función g


# Plotea salida de la función g


# Crear un intervalo de valores 'x' de -100 a 100


# Obtener los valores 'y' correspondientes de la función


# Configurar el gráfico


# Graficar 'x' contra g(x)


# Plotea un círculo vacío para mostrar el punto indefinido


 ```